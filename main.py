import os
import argparse
import requests
import pydicom
from pathlib import Path
import tarfile
from pydicom import config
config.enforce_valid_values = True
config.datetime_conversion = True
from datetime import datetime
from numpy import mean
import shutil
import sys, traceback

wd = os.getcwd()
temp_dir = os.path.join(wd, "Temp")
PATIENTS = "Patients"

class ScanTime:
    def __init__(self,sdate):
        date = datetime.strptime(sdate, '%H%M%S.%f')
        self.start = date
        self.end = date

    def update(self,sdate):
        date = datetime.strptime(sdate, '%H%M%S.%f')
        self.start = min(self.start, date)
        self.end = max(self.end, date)

    def difference(self):
        return (self.end - self.start).total_seconds()

class Patient:
    def __init__(self, name, age, sex):
        self.name = name
        self.age = age
        self.sex = sex

    def __repr__(self):
        return "Name: {} - Age: {}. Sex: {}".format(self.name, self.age, self.sex)

def clear_temp_dir():
    try:
        shutil.rmtree(temp_dir)
    except OSError as e:
        print("Error: %s : %s" % (temp_dir, e.strerror))

# Download the data according to user input
def download_data(url):
    filename = url.rsplit('/', 1)[1]
    response = requests.get(url, allow_redirects=True)
    Path(temp_dir).mkdir(parents=True, exist_ok=True)
    decomposed_path = os.path.join(temp_dir, filename)
    open(decomposed_path, 'wb').write(response.content)
    os.chdir(temp_dir)
    tar = tarfile.open(decomposed_path)
    tar.extractall()
    tar.close()
    os.chdir(wd)

# Storing the file according to the hierarchy
def move_file(subpath, src_path, file_name):
    series_path = os.path.join(PATIENTS, subpath)
    Path(series_path).mkdir(parents=True, exist_ok=True)
    os.replace(src_path, os.path.join(series_path, file_name))

# Gather info for Q1-3
def gather_info(ds, series_identifier, series, hospitals_set, patients_dict):
    patient_name = str(ds.PatientName)
    if series_identifier not in series.keys():
        series[series_identifier] = ScanTime(str(ds.AcquisitionTime))
        patient_age = ds.PatientAge
        patient_sex = ds.PatientSex
        hospital = ds.InstitutionName
        hospitals_set.add(hospital)
        patients_dict[patient_name] = Patient(patient_name, patient_age, patient_sex)
    else:
        series[series_identifier].update(str(ds.AcquisitionTime))

def print_results(series, patients_dict, hospitals_set):
    avg_scan_duration = mean([s.difference() for s in series.values()])
    patients_list = list(patients_dict.values())

    print("\nPrinting Results:\nQ1. Patients:\n" + str(patients_list))
    print("Q2. Average scan duration: " + str(avg_scan_duration))
    print("Q3. Number of different hospitals in the data: " + str(len(hospitals_set)))

def arrange_and_explore_data():
    hospitals_set = set()
    patients_dict = {}
    series_dict = {}
    Path(PATIENTS).mkdir(parents=True, exist_ok=True)
    files = Path(temp_dir).rglob('*.dcm')
    for instance in files:
        instance_path = str(instance)
        ds = pydicom.dcmread(instance_path)
        inst_name = os.path.basename(instance_path)
        patient_name = str(ds.PatientName)
        study_id = str(ds.StudyInstanceUID)
        series_id = str(ds.SeriesInstanceUID)
        series_identifier = os.path.join(patient_name, study_id, series_id)

        move_file(series_identifier, instance_path, inst_name)

        gather_info(ds, series_identifier, series_dict, hospitals_set, patients_dict)

    print_results(series_dict, patients_dict, hospitals_set)

def main():
    args = vars(argsp.parse_args())
    try:
        print("Downloading the data")
        download_data(args['url'])
    except Exception as e:
        traceback.print_exc(file=sys.stdout)
        print("Your url might be wrong or not supported. Exiting...")
        return
    print("Finished download the data")
    # Arranges the data by the structure: patient/study/series and answering the questions
    arrange_and_explore_data()
    clear_temp_dir()

if __name__ == "__main__":
    argsp = argparse.ArgumentParser()
    argsp.add_argument("-u", "--url", required=True, help="Url to download data from")
    main()
